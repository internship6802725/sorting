import unittest
from sorting import bubble_sort, quick_sort, bubble_sort_step, quick_sort_step

class TestSorting(unittest.TestCase):

    def test_bubble_sort(self):
        self.assertEqual(bubble_sort([1, 2, 3, 4, 5]), [1, 2, 3, 4, 5])
        self.assertEqual(bubble_sort([5, 4, 3, 2, 1]), [1, 2, 3, 4, 5])
        self.assertEqual(bubble_sort([3, 1, 4, 5, 2]), [1, 2, 3, 4, 5])
        self.assertEqual(bubble_sort([1]), [1])
        self.assertEqual(bubble_sort([]), [])
        self.assertEqual(bubble_sort([2, 2, 2, 2, 2]), [2, 2, 2, 2, 2])
        self.assertEqual(bubble_sort([5, 3, 8, 6, 2, 7, 4, 1, 9, 0]), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.assertEqual(bubble_sort([i for i in range(100)]), [i for i in range(100)])
        self.assertEqual(bubble_sort([i for i in range(100, 0, -1)]), [i for i in range(1, 101)])
        self.assertEqual(bubble_sort([-1, -3, -2, 0, 2, 1, -4]), [-4, -3, -2, -1, 0, 1, 2])
        self.assertEqual(bubble_sort([4, 1, 3, 2, 2, 4, 5, 3, 1]), [1, 1, 2, 2, 3, 3, 4, 4, 5])

    def test_bubble_sort_step(self):
        # Test 1: No swaps needed (already sorted array)
        array = [1, 2, 3, 4, 5]
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [1, 2, 3, 4, 5])
        self.assertFalse(swapped)

        # Test 2: One swap needed
        array = [2, 1, 3, 4, 5]
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [1, 2, 3, 4, 5])
        self.assertTrue(swapped)

        # Test 3: Multiple swaps needed
        array = [5, 1, 4, 2, 8]
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [1, 4, 2, 5, 8])
        self.assertTrue(swapped)

        # Test 4: Already sorted array
        array = [1, 2, 3, 4, 5]
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [1, 2, 3, 4, 5])
        self.assertFalse(swapped)

        # Test 5: Reverse sorted array
        array = [5, 4, 3, 2, 1]
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [4, 3, 2, 1, 5])
        self.assertTrue(swapped)

        # Test 6: Empty array
        array = []
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [])
        self.assertFalse(swapped)

        # Test 7: Single element array
        array = [1]
        length = len(array)
        sorted_array, swapped = bubble_sort_step(array, length)
        self.assertEqual(sorted_array, [1])
        self.assertFalse(swapped)

    def test_quick_sort(self):
        self.assertEqual(quick_sort([1, 2, 3, 4, 5]), [1, 2, 3, 4, 5])
        self.assertEqual(quick_sort([5, 4, 3, 2, 1]), [1, 2, 3, 4, 5])
        self.assertEqual(quick_sort([3, 1, 4, 5, 2]), [1, 2, 3, 4, 5])
        self.assertEqual(quick_sort([1]), [1])
        self.assertEqual(quick_sort([]), [])
        self.assertEqual(quick_sort([2, 2, 2, 2, 2]), [2, 2, 2, 2, 2])
        self.assertEqual(quick_sort([5, 3, 8, 6, 2, 7, 4, 1, 9, 0]), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.assertEqual(quick_sort([i for i in range(100)]), [i for i in range(100)])
        self.assertEqual(quick_sort([i for i in range(100, 0, -1)]), [i for i in range(1, 101)])
        self.assertEqual(quick_sort([-1, -3, -2, 0, 2, 1, -4]), [-4, -3, -2, -1, 0, 1, 2])
        self.assertEqual(quick_sort([4, 1, 3, 2, 2, 4, 5, 3, 1]), [1, 1, 2, 2, 3, 3, 4, 4, 5])

    def test_quick_sort_step(self):
        # Test 1: Basic case with distinct elements
        array = [10, 80, 30, 90, 40]
        #       [1, 1, 8, 10, 3, 2, 6]
        sorted_array, pivot_index = quick_sort_step(array)
        expected_array = [10, 30, 40, 90, 80]
        self.assertEqual(sorted_array, expected_array)
        #self.assertEqual(pivot_index, 3)  # The pivot index may vary based on implementation details

        # Test 2: Already sorted array
        array = [1, 2, 3, 4, 5]
        sorted_array, pivot_index = quick_sort_step(array)
        self.assertEqual(sorted_array, [1, 2, 3, 4, 5])
        self.assertTrue(0 <= pivot_index <= len(array))

        # Test 4: All elements are the same
        array = [2, 2, 2, 2, 2]
        sorted_array, pivot_index = quick_sort_step(array)
        self.assertEqual(sorted_array, [2, 2, 2, 2, 2])
        self.assertEqual(pivot_index, len(array))

        # Test 6: Single element array
        array = [1]
        sorted_array, pivot_index = quick_sort_step(array)
        self.assertEqual(sorted_array, [1])
        self.assertEqual(pivot_index, 1)


if __name__ == '__main__':
    unittest.main()